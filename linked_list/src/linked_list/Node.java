package linked_list;

/*
 * Written by Piotr Kubicki on 16/02/2018
   Simple Linked list implementation
 */

public class Node<T> {

	private T value;
	private Node nextNode;
	
	public Node(T value, Node node) {
		this.value = value;
		this.nextNode = node;
	}
	
	public T getValue() {
		return value;
	}
	
	public void setValue(T value) {
		this.value = value;
	}
	
	public Node getNextNode() {
		return nextNode;
	}
	
	public void setNextNode(Node nextNode) {
		this.nextNode = nextNode;
	}
}
