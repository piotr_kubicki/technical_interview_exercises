package linked_list;

import java.util.Scanner;

public class Application {

	public static void main(String[] args) {
		String c = null;
		LinkedList ll = new LinkedList();	
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Welcome to Linked List demonstration program!");
		
		do {
			System.out.println("I : Insert, R : Remove, Q : Quit");
			c = scan.nextLine();
			
			switch (c.toLowerCase()) {
			case "i": System.out.print("Insert numeric value: ");
				ll.insert(Integer.parseInt(scan.nextLine()));
				ll.print();
				break;
			case "r": System.out.print("Insert value to remove: ");
				Node n = ll.getNode(Integer.parseInt(scan.nextLine()));
				ll.remove(n);
				ll.print();
			case "q": break;
			default: System.out.println(c + " Command not found!");
			}
		} while (!c.equals("q")); 
		
		System.out.println("Thank you, bye!");
	}

}
