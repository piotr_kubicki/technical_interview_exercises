package linked_list;

public class LinkedList {
	Node n;
	
	public LinkedList() {
		n = null;
	}

	public void insert(int value) {
		Node last = n;
		
		if (last == null) {
			n = new Node(value, null);
		} else {
			while (last.getNextNode() != null) {
				last = last.getNextNode();
			}
			
			Node nNode = new Node(value, null);
			last.setNextNode(nNode);
		}
	}
	
	public <T> Node getNode(T value) {
		Node current = n;
		
		while (current != null) {
			if (current.getValue() == value || current.getValue().equals(value)) {
				return current;
			}
			
			current = current.getNextNode();
		}
		
		return null;
	}
	
	public Node remove(Node node) {
		Node prev = null;
		Node current = n;
		
		while (current != null) {
			if (current == node) {
				if (prev != null) {
					prev.setNextNode(current.getNextNode());
				} else {
					n = n.getNextNode();
				}
				return current;
			}
			prev = current;
			current = current.getNextNode();
		}
		
		return null;
	}
	
	public void print() {
		Node node = n;
		
		while (node != null) {
			System.out.println(node.getValue());
			node = node.getNextNode();
		}
	}
}
